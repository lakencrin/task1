#pragma once

#include "Utils.h"

#include <string>

#include <Windows.h>
#include <WinBase.h>
#include <Shlwapi.h>

class CommandLineParser
{
public:
	CommandLineParser(int argc, wchar_t* argv[])
	{
		if (argc < 2) 
			throw "No command presented";

		std::wstring command = argv[1];

		if (command == L"enable")
			_type = CommandType::Enable;
		else if (command == L"disable")
			_type = CommandType::Disable;
		else
			throw L"No such command \"" + command + L"\"";

		for (int i = 2; i < argc; )
		{
			if (std::wstring{ argv[i] } == L"--dir")
			{
				std::wstring directory = argv[i + 1];
				i += 2;

				if (PathIsRelativeW(directory.c_str()))
				{
					wchar_t work_dir[1000];
					GetCurrentDirectoryW(1000, work_dir);

					wchar_t buffer[1000];
					PathCombineW(buffer, work_dir, directory.c_str());
					_directory.assign(buffer);
				}
				else
					_directory = directory;
			}
		}
	}

	enum class CommandType
	{
		Disable = 0,
		Enable = 1,
	};

	CommandType type() const { return _type; }
	std::wstring directory() const { return _directory; }

private:
	CommandType _type;
	std::wstring _directory;
};

