#include "TemplateService.h"
#include "Utils.h"

#include <fstream>

#include <Shlwapi.h>
#include <AclAPI.h>

class Blocker
{
	const std::wstring& _file;

public:
	~Blocker()
	{
		wchar_t current_user[] = L"CURRENT_USER";
		Utils::addAceToObjectsSecurityDescriptor(const_cast<wchar_t*>(_file.data()), SE_FILE_OBJECT, current_user, TRUSTEE_IS_NAME, GENERIC_ALL, DENY_ACCESS, 0);
	}

	Blocker(const std::wstring& file)
		: _file{ file }
	{
		PACL dacl;
		DWORD result = GetNamedSecurityInfoW(_file.c_str(), SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, &dacl, nullptr, nullptr);

		ULONG count;
		PEXPLICIT_ACCESS_W entries;
		result = GetExplicitEntriesFromAclW(dacl, &count, &entries);

		for (uint32_t i = 0; i < count; ++i)
		{
			EXPLICIT_ACCESS_W& entry = entries[i];
			SID* pSid = reinterpret_cast<SID*>(entry.Trustee.ptstrName);
			wchar_t name[1000]{}, domain[1000]{};
			DWORD cchName = 1000, cchRefDomName = 1000;
			SID_NAME_USE name_use;
			BOOL result = LookupAccountSidW(nullptr, pSid, name, &cchName, domain, &cchRefDomName, &name_use);
			if (result && name == Utils::currentUser() && entry.grfAccessMode == DENY_ACCESS)
				DeleteAce(dacl, i);
		}

		std::wstring buffer = _file;
		result = SetNamedSecurityInfoW(const_cast<wchar_t*>(buffer.data()), SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, dacl, nullptr);
	}
};

TemplateService::TemplateService(Type type, const std::wstring& templateFilePath)
	: _path{ Utils::combinePaths(templateFilePath, L"template.tbl") }
	, _type{ type }
{
	if (!PathFileExistsW(_path.c_str()))
	{
		CreateFileW(_path.c_str(), 0, 0, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);

		wchar_t current_user[] = L"CURRENT_USER";
		Utils::addAceToObjectsSecurityDescriptor(const_cast<wchar_t*>(_path.data()), SE_FILE_OBJECT, current_user, TRUSTEE_IS_NAME, GENERIC_ALL, DENY_ACCESS, 0);
	}
}

TemplateService::~TemplateService() = default;

void TemplateService::read() 
{
	Blocker blocker{ _path };

	std::wfstream file;
	file.open(_path, std::ios_base::in);
	std::getline(file, _hashed_pass);
	while (!file.eof())
	{
		std::wstring mask_raw;
		std::getline(file, mask_raw);

		if (!mask_raw.empty())
			_masks.emplace_back(mask_raw);
	}
}
void TemplateService::write() 
{
	Blocker blocker{ _path };

	std::wfstream file;
	file.open(_path, std::ios_base::out);
	file << _hashed_pass << std::endl;
	for (auto& mask : _masks)
		file << mask.representation() << std::endl;
}

size_t TemplateService::maskCount() const 
{
	return _masks.size();
}
Mask TemplateService::mask(size_t index) const 
{
	return _masks[index];
}
TemplateService::MaskList TemplateService::masks() const 
{
	return _masks;
}
std::wstring TemplateService::passwordHash() const
{
	return _hashed_pass;
}

void TemplateService::addMask(const std::wstring& mask)
{
	_masks.emplace_back(mask);
}
void TemplateService::setPasswordHash(const std::wstring& password)
{
	_hashed_pass = password;
}