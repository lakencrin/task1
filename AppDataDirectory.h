#pragma once

#include <string>

class AppDataDirectory
{
public:
	static AppDataDirectory* instance();

	AppDataDirectory();

	bool hasChild(const std::wstring& childName);
	std::wstring createChildDirectory(const std::wstring& childName);

private:
	std::wstring _path;
};

