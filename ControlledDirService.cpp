#include "ControlledDirService.h"
#include "Utils.h"
#include "TemplateService.h"
#include "AppDataDirectory.h"

#include <AclAPI.h>

const uint32_t file_mask = 0x000d0f36;
const uint32_t dir_mask = 0x000d0f16;

ControlledDirService::ControlledDirService(const std::wstring& dir_path)
	: _path{ dir_path }
{
}

TemplateService* ControlledDirService::templateService(TemplateService::Type type)
{
	if (!_service)
	{
		std::wstring dirHash = Utils::narrowToWide(Utils::hash(_path));
		std::wstring dir = AppDataDirectory::instance()->createChildDirectory(dirHash);

		_service.reset(new TemplateService{ type, dir });
	}
	return _service.get();
}


bool ControlledDirService::isBlocked() const
{
	PACL dacl;
	DWORD result = GetNamedSecurityInfoW(_path.c_str(), SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, &dacl, nullptr, nullptr);

	ULONG count;
	PEXPLICIT_ACCESS_W entries;
	result = GetExplicitEntriesFromAclW(dacl, &count, &entries);

	for (uint32_t i = 0; i < count; ++i)
	{
		EXPLICIT_ACCESS_W& entry = entries[i];
		SID* pSid = reinterpret_cast<SID*>(entry.Trustee.ptstrName);
		wchar_t name[1000]{}, domain[1000]{};
		DWORD cchName = 1000, cchRefDomName = 1000;
		SID_NAME_USE name_use;
		BOOL result = LookupAccountSidW(nullptr, pSid, name, &cchName, domain, &cchRefDomName, &name_use);
		if (result && name == Utils::currentUser() && entry.grfAccessMode == DENY_ACCESS)
			return true;
	}
	return false;
}

void ControlledDirService::block()
{
	std::wstring path = _path;
	wchar_t current_user[] = L"CURRENT_USER";


	TemplateService::MaskList masks = _service->masks();
	std::vector<std::wstring> filesToBlock;
	for (auto& file : childFiles())
	{
		for (auto& mask : _service->masks())
		{
			if (mask.isMatched(file))
			{
				filesToBlock.emplace_back(file);
				break;
			}
		}
	}

	for (auto& file : filesToBlock)
	{
		std::wstring const fullPath = Utils::combinePaths(_path, file);
		Utils::addAceToObjectsSecurityDescriptor(const_cast<wchar_t*>(fullPath.data()), SE_FILE_OBJECT, current_user, TRUSTEE_IS_NAME, file_mask, DENY_ACCESS, 0);
	}

	Utils::addAceToObjectsSecurityDescriptor(const_cast<wchar_t*>(path.data()), SE_FILE_OBJECT, current_user, TRUSTEE_IS_NAME, dir_mask, DENY_ACCESS, 0);
}

void ControlledDirService::unblock()
{
	ControlledDirService::unblockSingleFile(_path);

	for (auto& child : childFiles())
	{
		for (auto& mask : _service->masks())
		{
			if (mask.isMatched(child))
			{
				ControlledDirService::unblockSingleFile(Utils::combinePaths(_path, child));
				break;
			}
		}
	}
	RemoveDirectoryW(_path.c_str());
}

std::vector<std::wstring> ControlledDirService::childFiles() const
{
	std::vector<std::wstring> result;

	WIN32_FIND_DATAW data;
	HANDLE search_handle = FindFirstFileW((_path + L"\\*").c_str(), &data);
	if (search_handle != INVALID_HANDLE_VALUE)
	{
		result.emplace_back(data.cFileName);

		while (FindNextFileW(search_handle, &data))
			result.emplace_back(data.cFileName);
	}

	return result;
}

void ControlledDirService::unblockSingleFile(const std::wstring& fileName)
{
	PACL dacl;
	DWORD result = GetNamedSecurityInfoW(fileName.c_str(), SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, &dacl, nullptr, nullptr);

	ULONG count;
	PEXPLICIT_ACCESS_W entries;
	result = GetExplicitEntriesFromAclW(dacl, &count, &entries);

	for (uint32_t i = 0; i < count; ++i)
	{
		EXPLICIT_ACCESS_W& entry = entries[i];
		//std::wcout << entry.grfAccessPermissions << std::hex << std::endl;
		SID* pSid = reinterpret_cast<SID*>(entry.Trustee.ptstrName);
		//std::wcout << pSid.Revision << std::endl;
		wchar_t name[1000]{}, domain[1000]{};
		DWORD cchName = 1000, cchRefDomName = 1000;
		SID_NAME_USE name_use;
		//BOOL result = LookupAccountNameW(nullptr, nullptr, &pSid, &cbSid, buffer, &cchRefDomName, &name_use);
		BOOL result = LookupAccountSidW(nullptr, pSid, name, &cchName, domain, &cchRefDomName, &name_use);
		if (result && name == Utils::currentUser() && entry.grfAccessMode == DENY_ACCESS)
			DeleteAce(dacl, i);
	}

	std::wstring buffer = fileName;
	result = SetNamedSecurityInfoW(const_cast<wchar_t*>(buffer.data()), SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, dacl, nullptr);
}