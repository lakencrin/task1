#pragma once

#include <fstream>
#include <string>
#include <vector>
#include <regex>

class Mask
{
	std::wstring _repr;
	std::wstring _raw;
public:
	Mask(std::wstring mask)
	{
		_repr = mask;
		_raw = checkForWildcards(mask) ? std::move(Mask::replace(mask)) : std::move(mask);
	}

	bool isWildcard() const
	{
		return checkForWildcards(_raw);
	}
	bool isMatched(const std::wstring& filename) const
	{
		if (!isWildcard())
			return filename == _raw;

		std::wregex regex{ _raw };
		return std::regex_match(filename, regex);
	}

	std::wstring representation() const
	{
		return _repr;
	}

private:
	static bool checkForWildcards(const std::wstring& string)
	{
		return string.find(L'?') != -1 || string.find(L'*') != -1;
	}
	static std::wstring& replace(std::wstring& mask)
	{
		size_t index = 0;
		wchar_t pointReplace[] = L"\\.";
		while (true)
		{
			index = mask.find(L'.', index);
			if (index == -1)
				break;
			mask.replace(mask.begin() + index, mask.begin() + index + 1, pointReplace, pointReplace + 2);
			index += 2;
		}
		index = 0;
		wchar_t starReplace[] = L".*";
		while (true)
		{
			index = mask.find(L'*', index);
			if (index == -1)
				break;
			mask.replace(mask.begin() + index, mask.begin() + index + 1, starReplace, starReplace + 2);
			index += 2;
		}
		index = 0;
		wchar_t questionMarkReplace[] = L".";
		while (true)
		{
			index = mask.find(L'?', index);
			if (index == -1)
				break;
			mask.replace(mask.begin() + index, mask.begin() + index + 1, questionMarkReplace, questionMarkReplace + 1);
			++index;
		}
		return mask;
	}
};

class TemplateService
{
	std::wstring _path;

public:
	using MaskList = std::vector<Mask>;

	enum class Type {
		Read,
		Write,
	} _type;

	TemplateService(Type type, const std::wstring& templateFilePath);
	~TemplateService();

	void read();
	void write();

	size_t maskCount() const;
	Mask mask(size_t index) const;
	MaskList masks() const;

	void addMask(const std::wstring& mask);
	void setPasswordHash(const std::wstring& password);
	std::wstring passwordHash() const;

private:
	MaskList _masks;
	std::wstring _hashed_pass;
};