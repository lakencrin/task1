#define _CRT_SECURE_NO_WARNINGS
#include "AppDataDirectory.h"
#include "Utils.h"

#include <iostream>

#include <Shlwapi.h>

static AppDataDirectory* _instance = nullptr;

AppDataDirectory* AppDataDirectory::instance()
{
	if (!_instance)
		_instance = new AppDataDirectory;
	return _instance;
}


AppDataDirectory::AppDataDirectory()
{
	wchar_t const appname[] = L"techmethprog-task1-1";
	wchar_t appdata_dir_raw[1000];
	std::wstring appdata_dir = PathCombineW(appdata_dir_raw, _wgetenv(L"APPDATA"), appname);
	if (FALSE == PathFileExistsW(appdata_dir.c_str()))
		CreateDirectoryW(appdata_dir.c_str(), nullptr);

	_path = appdata_dir;
}

bool AppDataDirectory::hasChild(const std::wstring& childName)
{
	std::wstring path = Utils::combinePaths(_path, childName);
	return FALSE != PathFileExistsW(path.c_str());
}

std::wstring AppDataDirectory::createChildDirectory(const std::wstring& childName)
{
	std::wstring dir = Utils::combinePaths(_path, childName);
	CreateDirectoryW(dir.c_str(), nullptr);
	return dir;
}