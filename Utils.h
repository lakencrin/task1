#pragma once

#include <string>

#include <AccCtrl.h>

namespace Utils
{
	bool checkAccess(const std::wstring& dir_path);

	DWORD addAceToObjectsSecurityDescriptor(
		LPTSTR pszObjName,          // name of object
		SE_OBJECT_TYPE ObjectType,  // type of object
		LPTSTR pszTrustee,          // trustee for new ACE
		TRUSTEE_FORM TrusteeForm,   // format of trustee structure
		DWORD dwAccessRights,       // access mask for new ACE
		ACCESS_MODE AccessMode,     // type of ACE
		DWORD dwInheritance         // inheritance flags for new ACE
	);

	std::string hash(const std::wstring& string);

	std::wstring combinePaths(const std::wstring& first, const std::wstring& second);

	std::wstring narrowToWide(const std::string& string);

	std::wstring currentUser();
}