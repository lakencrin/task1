#define _CRT_SECURE_NO_WARNINGS

#include "keccak.h"
#include "CommandLineParser.h"
#include "AppDataDirectory.h"
#include "TemplateService.h"
#include "ControlledDirService.h"
#include "PasswordGenerator.h"

#include <7zpp/7zpp.h>

#include <vector>
#include <iostream>

#include <stdio.h>

#include <Windows.h>
#include <WinBase.h>
#include <AclAPI.h>

int wmain(int argc, wchar_t* argv[])
{
	try
	{
		CommandLineParser parser{ argc, argv };

		if (!PathFileExistsW(parser.directory().c_str()))
			throw L"Given directory is not given";

		if (!Utils::checkAccess(parser.directory()))
			throw L"Given directory is not accessible";

		Keccak keccak;
		keccak.add(parser.directory().data(), 2 * parser.directory().length());
		std::string hash =  keccak.getHash();

		ControlledDirService service{ parser.directory() };

		TemplateService* templateService = service.templateService();
		if (parser.type() == CommandLineParser::CommandType::Enable)
		{
			if (service.isBlocked())
				throw L"Directory is already blocked";

			std::wcout << L"Enter files (or masks) to disable access to:\n";
			std::vector<std::wstring> templates;
			while (true)
			{
				std::wstring input;
				std::getline(std::wcin, input);
				templateService->addMask(input);

				if (input.empty())
					break;
			}

			std::wstring pass = Utils::narrowToWide(PasswordGenerator{}.generate());
			std::wstring hashPassword = Utils::narrowToWide(Utils::hash(pass));
			templateService->setPasswordHash(hashPassword);

			std::wcout << L"Password to disable block: " << pass << std::endl;
			templateService->write();
			service.block();
		}
		else if (parser.type() == CommandLineParser::CommandType::Disable)
		{
			if (!service.isBlocked())
				throw L"Directory is not blocked";

			templateService->read();

			std::cout << "Enter password to unblock: ";
			std::wstring password;
			std::wcin >> password;

			std::wstring origPasswordHash = templateService->passwordHash();
			std::wstring passwordHash = Utils::narrowToWide(Utils::hash(password));
			if (passwordHash != origPasswordHash)
				throw L"Invalid password";

			service.unblock();
		}
	}
	catch (const wchar_t* error)
	{
		std::wcout << error << std::endl;
	}
	return 0;
	//PACL dacl;
	//DWORD result = GetNamedSecurityInfoW(filename, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, &dacl, nullptr, nullptr);

	//ULONG count;
	//PEXPLICIT_ACCESS_W entries;
	//result = GetExplicitEntriesFromAclW(dacl, &count, &entries);

	//for (uint32_t i = 0; i < count; ++i)
	//{
	//    EXPLICIT_ACCESS_W& entry = entries[i];
	//    //std::wcout << entry.grfAccessPermissions << std::hex << std::endl;
	//    SID* pSid = reinterpret_cast<SID*>(entry.Trustee.ptstrName);
	//    //std::wcout << pSid.Revision << std::endl;
	//    DWORD cbSid = sizeof(SID);
	//    wchar_t name[1000]{}, domain[1000]{};
	//    DWORD cchName = 1000, cchRefDomName = 1000;
	//    SID_NAME_USE name_use;
	//    //BOOL result = LookupAccountNameW(nullptr, nullptr, &pSid, &cbSid, buffer, &cchRefDomName, &name_use);
	//    BOOL result = LookupAccountSidW(nullptr, pSid, name, &cchName, domain, &cchRefDomName, &name_use);
	//    if (result)
	//        wprintf(L"%s %s\n", name, domain);
	//}
	//for (int i = count - 1; i >= 0; --i)
	//    DeleteAce(dacl, i);
	//result = SetNamedSecurityInfoW(filename, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, dacl, nullptr);
	//return 0;
	/*SevenZip::SevenZipLibrary lib;
	lib.Load();

	SevenZip::SevenZipCompressor compressor{ lib, L"archive.zip" };
	compressor.SetCompressionFormat(SevenZip::CompressionFormat::Zip);
	compressor.UseAbsolutePaths(false);
	compressor.AddFile(filename);
	compressor.DoCompress();*/

	/*PSID owner, group;
	PACL dacl;
	PSECURITY_DESCRIPTOR descriptor;

	wchar_t current_user[] = L"CURRENT_USER";
	DWORD result = AddAceToObjectsSecurityDescriptor(filename, SE_FILE_OBJECT, current_user, TRUSTEE_IS_NAME, 0x000d0f36, DENY_ACCESS, 0);
	return result;*/
}
