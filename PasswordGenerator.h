#pragma once

#include <string>
#include <random>

class PasswordGenerator
{
	size_t const _length = 50;
	std::string const charset = "!;%:?*()_+=-~/<>,.[]{}1234567890ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba";

public:
	std::string generate()
	{
		std::random_device rand;
		std::string pass;
		pass.reserve(_length);
		for (size_t i = 0; i < _length; ++i)
		{
			size_t index = static_cast<size_t>(rand() % charset.size());
			pass.push_back(charset[index]);
		}
		return pass;
	}

};

