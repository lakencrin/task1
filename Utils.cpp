#define _CRT_SECURE_NO_WARNINGS

#include "Utils.h"
#include "keccak.h"

#include <Shlwapi.h>
#include <AclAPI.h>

bool Utils::checkAccess(const std::wstring& dir_path)
{
	std::wstring const _dir_path = std::wstring{ dir_path.back() == L'\\' ? dir_path : (dir_path + L'\\') } + L'*';

	WIN32_FIND_DATAW data;
	DWORD error = GetLastError();
	HANDLE search_handle = FindFirstFileW(_dir_path.c_str(), &data);
	if (search_handle != INVALID_HANDLE_VALUE)
		return true;

	error = GetLastError();
	if (error == ERROR_ACCESS_DENIED)
		return false;
	if (error == ERROR_FILE_NOT_FOUND)
		throw L"Directory was not found";

	throw L"Unexpected error";
}

DWORD Utils::addAceToObjectsSecurityDescriptor(
	LPTSTR pszObjName,          // name of object
	SE_OBJECT_TYPE ObjectType,  // type of object
	LPTSTR pszTrustee,          // trustee for new ACE
	TRUSTEE_FORM TrusteeForm,   // format of trustee structure
	DWORD dwAccessRights,       // access mask for new ACE
	ACCESS_MODE AccessMode,     // type of ACE
	DWORD dwInheritance         // inheritance flags for new ACE
)
{
	DWORD dwRes = 0;
	PACL pOldDACL = NULL, pNewDACL = NULL;
	PSECURITY_DESCRIPTOR pSD = NULL;
	EXPLICIT_ACCESS ea;

	if (NULL == pszObjName)
		return ERROR_INVALID_PARAMETER;

	// Get a pointer to the existing DACL.

	dwRes = GetNamedSecurityInfo(pszObjName, ObjectType,
		DACL_SECURITY_INFORMATION,
		NULL, NULL, &pOldDACL, NULL, &pSD);
	if (ERROR_SUCCESS != dwRes) {
		printf("GetNamedSecurityInfo Error %u\n", dwRes);
		goto Cleanup;
	}

	// Initialize an EXPLICIT_ACCESS structure for the new ACE. 

	ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));
	ea.grfAccessPermissions = dwAccessRights;
	ea.grfAccessMode = AccessMode;
	ea.grfInheritance = dwInheritance;
	ea.Trustee.TrusteeForm = TrusteeForm;
	ea.Trustee.ptstrName = pszTrustee;

	// Create a new ACL that merges the new ACE
	// into the existing DACL.

	dwRes = SetEntriesInAcl(1, &ea, pOldDACL, &pNewDACL);
	if (ERROR_SUCCESS != dwRes) {
		printf("SetEntriesInAcl Error %u\n", dwRes);
		goto Cleanup;
	}

	// Attach the new ACL as the object's DACL.

	dwRes = SetNamedSecurityInfo(pszObjName, ObjectType,
		DACL_SECURITY_INFORMATION,
		NULL, NULL, pNewDACL, NULL);
	if (ERROR_SUCCESS != dwRes) {
		printf("SetNamedSecurityInfo Error %u\n", dwRes);
		goto Cleanup;
	}

Cleanup:

	if (pSD != NULL)
		LocalFree((HLOCAL)pSD);
	if (pNewDACL != NULL)
		LocalFree((HLOCAL)pNewDACL);

	return dwRes;
}

std::string Utils::hash(const std::wstring& string)
{
	Keccak keccak;
	keccak.add(string.c_str(), sizeof(std::wstring::value_type) * string.length());
	return keccak.getHash();
}

std::wstring Utils::combinePaths(const std::wstring& first, const std::wstring& second)
{
	std::wstring result;
	result.resize(first.length() + second.length() + 1i64);
	PathCombineW(const_cast<wchar_t*>(result.data()), first.c_str(), second.c_str());
	return result;
}

std::wstring Utils::narrowToWide(const std::string& string)
{
	wchar_t* buffer = new wchar_t[string.length() * 2]{};
	mbstowcs(buffer, string.c_str(), string.length());
	std::wstring result = buffer;
	delete[] buffer;
	return result;
}

std::wstring Utils::currentUser()
{
	wchar_t buffer[1000];
	DWORD size = 1000;
	GetUserNameW(buffer, &size);
	return buffer;
}