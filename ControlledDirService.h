#pragma once

#include "TemplateService.h"

#include <string>
#include <vector>
#include <memory>

class ControlledDirService
{
	std::wstring const _path;
	std::unique_ptr<class TemplateService> _service;

public:
	ControlledDirService(const std::wstring& dir_path);

	TemplateService* templateService(TemplateService::Type type = TemplateService::Type::Read);

	bool isBlocked() const;

	void block();
	void unblock();

	std::vector<std::wstring> childFiles() const;

private:
	static void unblockSingleFile(const std::wstring& fileName);
};

